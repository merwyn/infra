---

- name: Create git group
  group:
    name: git
    state: present

- name: Create git-static group
  group:
    name: git-static
    state: present

- name: Gitea user
  user:
    system: true
    password: '!'
    home: /home/git
    shell: /bin/bash
    comment: "Git Version Control"
    group: git
    name: git

- name: Gitea static user  # To compile and own static content
  user:
    system: true
    password: '!'
    comment: "To compile and own static gitea content."
    group: git-static
    name: git-static

- name: Download gitea
  get_url:
    dest: /usr/local/bin/gitea
    url: "https://dl.gitea.io/gitea/{{ gitea_version }}/gitea-{{ gitea_version }}-linux-amd64"
    mode: 0755
    owner: root
    group: root

- name: Download gitea upgrade script
  get_url:
    dest: /usr/local/bin/gitea-upgrade.sh
    url: "https://raw.githubusercontent.com/go-gitea/gitea/main/contrib/upgrade.sh"
    mode: 0755
    force: true
    owner: root
    group: root

- name: Install dependencies
  package:
    name:
      - git
      - postgresql
      - python3-psycopg2  # For Ansible
      # - rsync  # for static file generation
      # - nodejs # for static file generation
      # - npm    # for static file generation
      # - make   # for static file generation
      - nginx
      - jq  # For upgrade.sh
      - pandoc # For reStructuredText rendering
    state: present

- name: Ensure locale en_US.UTF-8 exists
  locale_gen:
    name: en_US.UTF-8
    state: present
  register: locale_gen_result

- name: Force-restart PostgreSQL after new locales are generated
  service:
    name: postgresql
    state: restarted
  when: locale_gen_result.changed

- name: Create psql git user
  become: true
  become_user: postgres
  postgresql_user:
    user: git

- name: Create psql gitea DB
  become: true
  become_user: postgres
  postgresql_db:
    name: gitea
    owner: git
    encoding: UTF-8
    lc_collate: en_US.UTF-8
    lc_ctype: en_US.UTF-8
    template: template0

- name: Create gitea hierarchy
  file:
    state: directory
    mode: 0750
    owner: git
    group: git
    path: "{{ item }}"
  loop:
    - /var/lib/gitea/data
    - /var/lib/gitea/log
    - /var/lib/gitea/custom/templates/custom/
    - /var/lib/gitea/custom/public/img/

- name: Create gitea config hierarchy
  file:
    state: directory
    mode: 0750
    owner: root
    group: git
    path: /etc/gitea

- name: Setup nginx
  include_role: name=nginx
  vars:
    nginx_domain: git.afpy.org
    nginx_certificates: ['git.afpy.org']
    nginx_conf: |
      server
      {
          listen [::]:80; listen 80;
          server_name git.afpy.org;
          return 301 https://git.afpy.org$request_uri;
      }

      server
      {
          listen [::]:443 ssl http2; listen 443 ssl http2;
          server_name git.afpy.org;
          include snippets/letsencrypt-git.afpy.org.conf;
          client_max_body_size 16M;

          add_header Content-Security-Policy-Report-Only "default-src 'self'; connect-src 'self'; font-src 'self' data:; form-action 'self'; img-src 'self' https: data:; manifest-src 'self' data:; object-src 'self'; script-src 'self' 'unsafe-eval' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; worker-src 'self'";
          # See add_header Content-Security-Policy-Report-Only "
          add_header X-Content-Type-Options "nosniff";

          # location /_/static/assets/ {
          #     alias /var/lib/gitea-static/public/;
          # }

          location = /robots.txt {
              default_type "text/plain";
              return 200 "User-agent: ClaudeBot
      Disallow: /

      User-agent: facebookexternalhit
      Disallow: /";
          }

          location / {
              proxy_pass http://localhost:3000;
              proxy_set_header Host $host;
              proxy_set_header X-Real-IP $remote_addr;
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header X-Forwarded-Proto $scheme;
          }

      }


# Public asset generation (to allow nginx to serve them) needs nodejs>14.

# - name: Create gitea static hierarchy
#   file:
#     state: directory
#     mode: 0755
#     owner: git-static
#     group: git-static
#     path: "{{ item }}"
#   loop:
#     - /var/lib/gitea-static/source
#     - /var/lib/gitea-static/public
#
# - name: Download gitea tarball  # For the static content
#   unarchive:
#     src: "https://github.com/go-gitea/gitea/archive/refs/tags/v{{ gitea_version }}.tar.gz"
#     dest: /var/lib/gitea-static/source/
#     remote_src: true
#     owner: git-static
#     group: git-static
#   register: download_gitea_tarball
#
# - name: Compile static assets
#   command: make frontend
#   args:
#     chdir: "/var/lib/gitea-static/source/gitea-{{ gitea_version }}"
#   become: true
#   become_user: git-static
#   when: download_gitea_tarball is changed
#
# - name: Copy public assets
#   synchronize:
#     src: "/var/lib/gitea-static/source/gitea-{{ gitea_version }}/public/"
#     dest: "/var/lib/gitea-static/public/"
