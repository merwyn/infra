# Nginx with Letsencrypt

This role sets up nginx with letsencrypt (using DNS-01 with Gandi API) .


## Role Variables

The mandatory variables are:

- `admin_email`: For letsencrypt.
- `gandi_api_key` ([see doc](https://github.com/obynio/certbot-plugin-gandi/)).
- `nginx_certificates`: A list of domain to put in this certificate.
- `nginx_domain`: Used for file names, certificate name, and default server_name if no nginx_conf is given.
- `nginx_conf`: The nginx config.

Optional variables are:

- `nginx_owner`: If a unix user has to be created for this project.
- `nginx_path`: To create a directory owned by `nginx_owner`.
- `certbot_authenticator`: Defaults to `gandi`, can use `nginx`.

### Author Information

Julien Palard — https://mdk.fr
