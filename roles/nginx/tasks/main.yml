---

- name: Create SSL dhparam
  get_url:
    url: https://ssl-config.mozilla.org/ffdhe2048.txt
    dest: /etc/ssl/certs/dhparam.pem
    mode: 0644

- name: Setup or upgrade venv
  command: python3 -m venv --upgrade-deps /root/certbot-venv/
  changed_when: False

- name: Prepare certbot+gandi venv
  pip:
    chdir: /root/
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: /root/certbot-venv/
    name:
      - "pip>=21.0.1"
      - "setuptools>=53.0.0"
      - "wheel>=0.36.2"

- name: Install certbot+gandi in venv
  pip:
    chdir: /root/
    virtualenv_command: /usr/bin/python3 -m venv
    virtualenv: /root/certbot-venv/
    name:
      - "certbot<2"  # See https://github.com/certbot/certbot/issues/9485
      - "acme<2"  # See https://github.com/certbot/certbot/issues/9485
      - "certbot-plugin-gandi"
      - "certbot-nginx"  # In case there's a "certbot_authenticator == 'nginx'"

- name: Setup Gandi credentials
  copy:
    content: |
      dns_gandi_token = {{ gandi_personal_access_token }}
    mode: 0600
    dest: /root/gandi.ini
  when: certbot_authenticator == 'gandi'

- name: Generate TLS certificates via Gandi
  command: /root/certbot-venv/bin/certbot certonly --cert-name {{ nginx_domain | quote }} -n --agree-tos -d {{ nginx_certificates | join(",") | quote }} -m {{ admin_email | quote }} --authenticator dns-gandi --dns-gandi-credentials /root/gandi.ini
  register: certbot
  changed_when: '"no action taken." not in certbot.stdout'
  when: certbot_authenticator == 'gandi'

- name: Generate TLS certificates via nginx
  command: /root/certbot-venv/bin/certbot certonly --nginx --nginx-sleep-seconds 10 --cert-name {{ nginx_domain | quote }} -n --agree-tos -d {{ nginx_certificates | join(",") | quote }} -m {{ admin_email | quote }}
  register: certbot
  changed_when: '"no action taken." not in certbot.stdout'
  when: certbot_authenticator == 'nginx'

- name: Setup renewal cron
  cron:
    name: certbot
    minute: "55"
    hour: "8"
    job: '/root/certbot-venv/bin/certbot -q renew'

- name: Setup PATH in renewal cron
  ansible.builtin.cron:
    name: PATH
    env: yes
    job: "/usr/sbin:/usr/bin"

- name: Setup renewal hook directory
  file:
    path: /etc/letsencrypt/renewal-hooks/post
    state: directory
    mode: 0755

- name: Setup renewal hook script
  copy:
    dest: /etc/letsencrypt/renewal-hooks/deploy/nginx.sh
    mode: 0755
    content: |
      #!/bin/sh
      /usr/bin/systemctl reload nginx

- name: Install nginx
  package:
    state: present
    name:
      - nginx
      - ca-certificates

- name: Ensure certbot is not installed from Debian packages
  package:
    state: absent
    name:
      - certbot
      - python-certbot-nginx
      - python3-certbot-nginx

- name: Create letsencrypt snippets
  template:
    src: letsencrypt.conf.j2
    dest: '/etc/nginx/snippets/letsencrypt-{{ nginx_domain }}.conf'

- name: User
  user:
    system: true
    name: "{{ nginx_owner }}"
  when: nginx_owner is defined

- name: .ssh directory
  file:
    path: "~{{ nginx_owner }}/.ssh"
    state: directory
    owner: "{{ nginx_owner }}"
    mode: 0755
  when: nginx_owner is defined

- name: Deploy key
  blockinfile:
    create: true
    owner: "{{ nginx_owner }}"
    mode: 0644
    path: "~{{ nginx_owner }}/.ssh/authorized_keys"
    marker: "<!-- {mark} ANSIBLE MANAGED BLOCK: Deploy key for {{ nginx_domain }} -->"
    block: "{{ nginx_public_deploy_key }}"
  when: nginx_owner is defined and nginx_public_deploy_key is defined

- name: Configure nginx
  copy:
    content: "{{ nginx_conf }}"
    dest: "/etc/nginx/conf.d/{{ nginx_domain }}.conf"
  when: nginx_conf is defined and nginx_domain is defined
  notify: reload nginx

- name: WWW directory
  file:
    path: "{{ nginx_path }}"
    state: directory
    owner: "{{ nginx_owner }}"
    group: "{{ nginx_owner }}"
    mode: 0755
  when: nginx_owner is defined and nginx_path is defined

- name: Setup custom log format
  copy:
    dest: /etc/nginx/conf.d/logging.conf
    owner: root
    group: root
    mode: 0644
    content: |
      log_format custom '$host $remote_addr - $remote_user [$time_local] '
                        '"$request" $status $body_bytes_sent '
                        '"$http_referer" "$http_user_agent"';
      access_log /var/log/nginx/access.log custom;

- name: Hide logging setup from nginx.conf
  lineinfile:
    regex: access_log
    state: absent
    path: /etc/nginx/nginx.conf
    backup: true
